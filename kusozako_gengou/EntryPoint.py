
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from libkusozako3.Entity import DeltaEntity
from .FinderButton import DeltaFinderButton
from .model.Model import DeltaModel
from .box.Box import DeltaBox

MODEL = {
    "type": "simple-action",
    "title": _("Find"),
    "message": "delta > action",
    "user-data": (ApplicationSignals.FINDER_TOGGLE, None),
    "shortcut": "Ctrl+F",
    "close-on-clicked": True
    }


class DeltaEntryPoint(DeltaEntity):

    def _delta_info_filter_model(self):
        return self._model.filter_model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(self)
        DeltaBox(self)
        DeltaFinderButton(self)
        param = "main", MODEL
        self._raise("delta > application popover add item", param)
