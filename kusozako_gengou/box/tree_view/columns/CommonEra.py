
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Column import AlfaColumn


class DeltaCommonEra(AlfaColumn):

    TITLE = _("Common Era")
    TEXT_COLUMN = 3
