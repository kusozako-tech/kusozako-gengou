
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Column import AlfaColumn


class DeltaJapaneseEra(AlfaColumn):

    TITLE = _("Japanese Era")
    TEXT_COLUMN = 1
