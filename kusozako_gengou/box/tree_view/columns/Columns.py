
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CommonEra import DeltaCommonEra
from .JapaneseEra import DeltaJapaneseEra


class EchoColumns:

    def __init__(self, parent):
        DeltaCommonEra(parent)
        DeltaJapaneseEra(parent)
