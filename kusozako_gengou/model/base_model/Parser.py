
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import csv
from libkusozako3.Entity import DeltaEntity


class DeltaParser(DeltaEntity):

    def _read_rows(self, reader):
        for row in reader:
            self._raise("delta > append", row)

    def __init__(self, parent):
        self._parent = parent
        path = self._enquiry("delta > resource path", ".gengou.csv")
        with open(path, 'r') as csv_file:
            reader = csv.reader(csv_file)
            next(reader)
            self._read_rows(reader)
