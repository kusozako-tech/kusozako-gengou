
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from libkusozako3 import ApplicationSignals


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _on_map(self, search_entry):
        self.grab_focus()

    def _on_search_changed(self, search_entry):
        text = search_entry.get_text()
        param = ApplicationSignals.FINDER_KEYWORD_CHANGED, text
        self._raise("delta > action", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(self, margin_bottom=Unit(2))
        self.connect("map", self._on_map)
        self.connect("search-changed", self._on_search_changed)
        self._raise("delta > add to container", self)
