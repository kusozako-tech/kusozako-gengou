
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3 import ApplicationSignals
from libkusozako3.Entity import DeltaEntity
from .SearchEntry import DeltaSearchEntry


class DeltaFinder(Gtk.Revealer, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _action(self, param=None):
        revealed = not self.get_child_revealed()
        self.set_reveal_child(revealed)
        if not revealed:
            self._search_entry.props.text = ""

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == ApplicationSignals.FINDER_TOGGLE:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Revealer.__init__(self)
        self._search_entry = DeltaSearchEntry(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register action object", self)
