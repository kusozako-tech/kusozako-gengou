
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .columns.Columns import EchoColumns


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_append(self, tree_view_column):
        self.append_column(tree_view_column)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.TreeView.__init__(
            self,
            model=self._enquiry("delta > filter model")
            )
        EchoColumns(self)
        scrolled_window.add(self)
        self._raise("delta > add to container", scrolled_window)
