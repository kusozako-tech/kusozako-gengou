
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .Parser import DeltaParser


class DeltaBaseModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_append(self, row):
        self.append(row)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *(str,)*14)
        DeltaParser(self)
