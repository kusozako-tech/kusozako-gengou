
# (c) copyright 2021, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale

VERSION = "2022.08.02"
APPLICATION_NAME = "kusozako-gengou"
APPLICATION_ID = "com.gitlab.kusozako-tech.kusozako-gengou"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
    )

APPLICATION_DATA = {
    "name": APPLICATION_NAME,
    "id": APPLICATION_ID,
    "icon-name": APPLICATION_ID,
    "version": VERSION,
    "short description": _("Japanese era name reference"),
    "long-description": _("""Japanese era name reference""")
    }
