
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .StripeColors import DeltaStripeColors


class AlfaColumn(Gtk.TreeViewColumn, DeltaEntity):

    TITLE = "define title here."
    TEXT_COLUMN = 0

    def _cell_data_func(self, column, renderer, model, iter_, stripe_colors):
        renderer.set_property("height", Unit(6))
        renderer.set_property("xalign", 0.9)
        tree_path = model.get_path(iter_)
        is_even = (tree_path[0] % 2 == 0)
        stripe_colors.set_background(renderer, is_even)
        stripe_colors.set_foreground(renderer, is_even)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(ellipsize=Pango.EllipsizeMode.END)
        Gtk.TreeViewColumn.__init__(
            self,
            title=self.TITLE,
            cell_renderer=renderer,
            text=self.TEXT_COLUMN
            )
        stripe_colors = DeltaStripeColors(self)
        self.set_cell_data_func(renderer, self._cell_data_func, stripe_colors)
        self.set_expand(True)
        self._raise("delta > append", self)
