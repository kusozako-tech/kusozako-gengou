
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .finder.Finder import DeltaFinder
from .tree_view.TreeView import DeltaTreeView


class DeltaBox(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL
            )
        DeltaFinder(self)
        DeltaTreeView(self)
        self._raise("delta > add to container", self)
