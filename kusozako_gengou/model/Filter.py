
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3 import ApplicationSignals
from libkusozako3.Entity import DeltaEntity


class DeltaFilter(DeltaEntity):

    def _action(self, keyword):
        self._keyword = keyword
        self._raise("delta > request refilter")

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == ApplicationSignals.FINDER_KEYWORD_CHANGED:
            self._action(param)

    def _matches(self, tree_row):
        for data in tree_row:
            if self._keyword in data:
                return True
        return False

    def matches(self, tree_row):
        return True if self._keyword == "" else self._matches(tree_row)

    def __init__(self, parent):
        self._parent = parent
        self._keyword = ""
        self._raise("delta > register action object", self)
