
# (c) copyright 2022, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .base_model.BaseModel import DeltaBaseModel
from .Filter import DeltaFilter


class DeltaModel(DeltaEntity):

    def _delta_call_request_refilter(self):
        self._filter_model.refilter()

    def _visible_func(self, model, tree_iter, filter_):
        return filter_.matches(model[tree_iter])

    def __init__(self, parent):
        self._parent = parent
        base_model = DeltaBaseModel(self)
        filter_ = DeltaFilter(self)
        self._filter_model = base_model.filter_new(None)
        self._filter_model.set_visible_func(self._visible_func, filter_)

    @property
    def filter_model(self):
        return self._filter_model
